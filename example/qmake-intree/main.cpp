#include <QtCore/QCoreApplication>
#include <QtCore/QDate>
#include <QtOdf/SpreadsheetFile.h>

using namespace QtOdf;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    SpreadsheetFile spreadsheetFile;
    auto &styles = spreadsheetFile.automaticStyles();
    styles.defaultCurrencyStyle().setCurrencySymbol(QStringLiteral("¥"));

    auto &spreadsheet = spreadsheetFile.spreadsheet();
    auto &table = spreadsheet.table(QStringLiteral("Sheet #1"));

    table.cell(0, 0).setStringValue(QStringLiteral("Order Date"));
    table.cell(0, 1).setStringValue(QStringLiteral("Order Id"));
    table.cell(0, 2).setStringValue(QStringLiteral("Amount"));
    table.cell(0, 3).setStringValue(QStringLiteral("Value"));
    table.cell(0, 4).setStringValue(QStringLiteral("Percentage"));
    table.cell(0, 5).setStringValue(QStringLiteral("Shipped"));

    for(int row = 1; row < 10; row++) {
        table.cell(row, 0).setDateValue(QDate{2022, 1, 1});
        table.cell(row, 1).setStringValue(QStringLiteral("abdc"));
        table.cell(row, 2).setCurrencyValue(100.0);
        table.cell(row, 3).setFloatValue(3.14);
        table.cell(row, 4).setPercentageValue(0.50);
        table.cell(row, 5).setBooleanValue(true);
    }

    spreadsheetFile.save(QStringLiteral("example.ods"));

    a.quit();
    return a.exec();
}
