QT -= gui

CONFIG += console
CONFIG -= app_bundle

SOURCES += \
        main.cpp

# Include the QtOdf pri
include(../../QtOdf_intree.pri)
