#include <QtCore/QObject>
#include <QtCore/QRandomGenerator>
#include <QtCore/QUuid>
#include <QtTest/QTest>
#include <QtOdf/SpreadsheetFile.h>

using namespace QtOdf;

class WriteRandomOds : public QObject
{
    Q_OBJECT

private slots:
    void runTest() {
        SpreadsheetFile spreadsheetFile;
        auto &styles = spreadsheetFile.automaticStyles();
        styles.defaultTableColumnStyle().setUseOptimalColumnWidth(true);
        styles.defaultCurrencyStyle().setCurrencySymbol(QStringLiteral("¥"));

        auto &spreadsheet = spreadsheetFile.spreadsheet();
        auto &table = spreadsheet.table(QStringLiteral("Sheet#1"));

        table.cell(0, 0).setStringValue(QStringLiteral("Order Date"));
        table.cell(0, 1).setStringValue(QStringLiteral("Order Id"));
        table.cell(0, 2).setStringValue(QStringLiteral("Amount"));
        table.cell(0, 3).setStringValue(QStringLiteral("Value"));
        table.cell(0, 4).setStringValue(QStringLiteral("Percentage"));
        table.cell(0, 5).setStringValue(QStringLiteral("Shipped"));

        for(int row = 1; row < 10; row++) {
            table.cell(row, 0).setDateValue(QDate{2000, 1, 1}.addDays(QRandomGenerator::global()->bounded(7300)));
            table.cell(row, 1).setStringValue(QUuid::createUuid().toString());
            table.cell(row, 2).setCurrencyValue(QRandomGenerator::global()->bounded(100.0));
            table.cell(row, 3).setFloatValue(QRandomGenerator::global()->bounded(1000.0));
            table.cell(row, 4).setPercentageValue(QRandomGenerator::global()->bounded(1.0));
            table.cell(row, 5).setBooleanValue(QRandomGenerator::global()->bounded(2));
        }

        auto error = spreadsheetFile.save(QStringLiteral("random.ods"));
        QCOMPARE(error, OpenDocumentFile::Error::NoError);
    }
};

QTEST_MAIN(WriteRandomOds)
#include "WriteRandomOds.moc"
