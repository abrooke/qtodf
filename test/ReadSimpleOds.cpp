#include <QtCore/QObject>
#include <QtTest/QTest>
#include <QtOdf/SpreadsheetFile.h>

using namespace QtOdf;

class ReadSimpleOds : public QObject
{
    Q_OBJECT

private slots:
    void runTest() {
        SpreadsheetFile spreadsheetFile;
        auto error = spreadsheetFile.load(QStringLiteral("simple.ods"));
        QCOMPARE(error, OpenDocumentFile::Error::NoError);

        auto &spreadsheet = spreadsheetFile.spreadsheet();

        auto *tableByIndex = spreadsheet.tableAt(0);
        QVERIFY(tableByIndex);
        auto *tableByName = spreadsheet.tableAt(QStringLiteral("Feuille1"));
        QVERIFY(tableByName);
        QCOMPARE(tableByIndex, tableByName);

        auto &table = *tableByIndex;
        QCOMPARE(table.columnCount(), 2);
        QCOMPARE(table.rowCount(), 12);

        auto *cell01 = table.cellAt(0, 1);
        QVERIFY(cell01);
        QCOMPARE(cell01->valueType(), Table::TableCell::BOOLEAN);
        QCOMPARE(cell01->booleanValue(), true);

        auto *cell11 = table.cellAt(1, 1);
        QVERIFY(cell11);
        QCOMPARE(cell11->valueType(), Table::TableCell::CURRENCY);
        QCOMPARE(cell11->currencyValue(), 11.00);
        QCOMPARE(cell11->currencySymbol(), "EUR");

        auto *cell21 = table.cellAt(2, 1);
        QVERIFY(cell21);
        QCOMPARE(cell21->valueType(), Table::TableCell::DATE);
        QCOMPARE(cell21->dateValue(), QDate(2022, 01, 13));

        auto *cell31 = table.cellAt(3, 1);
        QVERIFY(cell31);
        QCOMPARE(cell31->valueType(), Table::TableCell::TIME);
        //QCOMPARE(cell31->booleanValue(), true);

        auto *cell41 = table.cellAt(4, 1);
        QVERIFY(cell41);
        QCOMPARE(cell41->valueType(), Table::TableCell::FLOAT);
        QCOMPARE(cell41->floatValue(), 11.00);

        auto *cell51 = table.cellAt(5, 1);
        QVERIFY(cell51);
        QCOMPARE(cell51->valueType(), Table::TableCell::PERCENTAGE);
        QCOMPARE(cell51->percentageValue(), 0.11);

        auto *cell61 = table.cellAt(6, 1);
        QVERIFY(cell61);
        QCOMPARE(cell61->valueType(), Table::TableCell::STRING);
        QCOMPARE(cell61->stringValue(), "Bonjour");

        auto *cell80 = table.cellAt(8, 0);
        QVERIFY(cell80);
        QCOMPARE(cell80->valueType(), Table::TableCell::STRING);
        QCOMPARE(cell80->stringValue(), "Merged cols");
        auto *cell81 = table.cellAt(8, 1);
        QVERIFY(cell81);
        QCOMPARE(cell80, cell81);
    }
};

QTEST_MAIN(ReadSimpleOds)
#include "ReadSimpleOds.moc"
