# QtOdf

A simplistic C++ Qt library for reading and writing OpenDocument spreadsheets (ods files)

## Features

- Read and write table cell values
- NO formatting/styling

## Dependencies

- Qt Core
- Qt Gui private headers (for build)

## Compiling & Installing

```bash
git clone https://gitlab.com/abrooke/qtodf.git && cd qtodf
mkdir build && cd build
cmake -DBUILD_TESTING:BOOL=NO -DCMAKE_BUILD_TYPE:STRING=Release ..
cmake --build .
cmake --install .
```

## Linking

### CMake

Add this to your `CMakeLists.txt` file:

```cmake
target_link_libraries(MyProject PRIVATE QtOdf)
```

### QMake

Add this to your project file:

```qmake
QT += core gui
CONFIG += c++17
LIBS += -lQtOdf
```

## Using QMake without install - compile sources in your project (not recommended)

Download QtOdf sources in your project source tree, and include the .pri in your project file.

```qmake
include(path/to/qtodf/QtOdf_intree.pri)
```

## Example code

```cpp
#include <QtCore/QCoreApplication>
#include <QtCore/QDate>
#include <QtOdf/SpreadsheetFile.h>

using namespace QtOdf;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    SpreadsheetFile spreadsheetFile;
    auto &styles = spreadsheetFile.automaticStyles();
    styles.defaultCurrencyStyle().setCurrencySymbol(QStringLiteral("¥"));

    auto &spreadsheet = spreadsheetFile.spreadsheet();
    auto &table = spreadsheet.table(QStringLiteral("Sheet #1"));

    table.cell(0, 0).setStringValue(QStringLiteral("Order Date"));
    table.cell(0, 1).setStringValue(QStringLiteral("Order Id"));
    table.cell(0, 2).setStringValue(QStringLiteral("Amount"));
    table.cell(0, 3).setStringValue(QStringLiteral("Value"));
    table.cell(0, 4).setStringValue(QStringLiteral("Percentage"));
    table.cell(0, 5).setStringValue(QStringLiteral("Shipped"));

    for(int row = 1; row < 10; row++) {
        table.cell(row, 0).setDateValue(QDate{2022, 1, 1});
        table.cell(row, 1).setStringValue(QStringLiteral("abdc"));
        table.cell(row, 2).setCurrencyValue(100.0);
        table.cell(row, 3).setFloatValue(3.14);
        table.cell(row, 4).setPercentageValue(0.50);
        table.cell(row, 5).setBooleanValue(true);
    }

    spreadsheetFile.save(QStringLiteral("example.ods"));

    a.quit();
    return a.exec();
}
```
