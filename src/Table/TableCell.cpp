#include <QtCore/QDate>
#include <QtCore/QDebug>
#include <QtCore/QLocale>
#include <QtOdf/Table/TableCell.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Table;

TableCell::TableCell()
    : _numberColumnsSpanned(1),
      _numberRowsSpanned(1),
      _valueType(VOID)
{
}

size_t TableCell::numberColumnsSpanned() const
{
    return _numberColumnsSpanned;
}

size_t TableCell::numberRowsSpanned() const
{
    return _numberRowsSpanned;
}

TableCell::ValueType TableCell::valueType() const
{
    return _valueType;
}

QString TableCell::valueTypeString() const
{
    return valueTypeToString(_valueType);
}

bool TableCell::booleanValue(bool *ok) const
{
    if(_valueType == BOOLEAN) {
        if(_attributes.hasAttribute(Attributes::OFFICE_BOOLEANVALUE)) {
            const auto value = _attributes.value(Attributes::OFFICE_BOOLEANVALUE);
            if(value == Values::TRUE) {
                if(ok) *ok = true;
                return true;
            }
            else if(value == Values::FALSE) {
                if(ok) *ok = true;
                return false;
            }
        }
    }

    if(ok) *ok = false;
    return false;
}

double TableCell::currencyValue(bool *ok) const
{
    if(_valueType == CURRENCY) {
        if(_attributes.hasAttribute(Attributes::OFFICE_VALUE)) {
            const auto value = _attributes.value(Attributes::OFFICE_VALUE);
            return value.toDouble(ok);
        }
    }

    if(ok) *ok = false;
    return false;
}

QString TableCell::currencySymbol(bool *ok) const
{
    if(_valueType == CURRENCY) {
        if(_attributes.hasAttribute(Attributes::OFFICE_CURRENCY)) {
            if(ok) *ok = true;
            return _attributes.value(Attributes::OFFICE_CURRENCY).toString();
        }
    }

    if(ok) *ok = false;
    return {};
}

QDate TableCell::dateValue() const
{
    if(_valueType == DATE) {
        if(_attributes.hasAttribute(Attributes::OFFICE_DATEVALUE)) {
            return QDate::fromString(_attributes.value(Attributes::OFFICE_DATEVALUE).toString(), Qt::ISODate);
        }
    }

    return {};
}

double TableCell::floatValue(bool *ok) const
{
    if(_valueType == FLOAT) {
        if(_attributes.hasAttribute(Attributes::OFFICE_VALUE)) {
            const auto value = _attributes.value(Attributes::OFFICE_VALUE);
            return value.toDouble(ok);
        }
    }

    if(ok) *ok = false;
    return false;
}

double TableCell::percentageValue(bool *ok) const
{
    if(_valueType == PERCENTAGE) {
        if(_attributes.hasAttribute(Attributes::OFFICE_VALUE)) {
            const auto value = _attributes.value(Attributes::OFFICE_VALUE);
            return value.toDouble(ok);
        }
    }

    if(ok) *ok = false;
    return false;
}

QString TableCell::stringValue(bool *ok) const
{
    if(_valueType == STRING) {
        if(ok) *ok = true;
        return _content.text();
    }

    if(ok) *ok = false;
    return {};
}

void TableCell::setBooleanValue(bool booleanValue)
{
    _valueType = BOOLEAN;
    _content.clearText();
    _attributes.clear();
    _attributes.reserve(3);
    _attributes.append(Attributes::OFFICE_VALUETYPE, Values::BOOLEAN);
    _attributes.append(Attributes::OFFICE_BOOLEANVALUE, booleanValue ? Values::TRUE : Values::FALSE);
    _attributes.append(Attributes::TABLE_STYLENAME, DefaultStyleNames::BOOLEAN);
}

void TableCell::setCurrencyValue(double numericValue)
{
    _valueType = CURRENCY;
    _content.clearText();
    _attributes.clear();
    _attributes.reserve(3);
    _attributes.append(Attributes::OFFICE_VALUETYPE, Values::CURRENCY);
    _attributes.append(Attributes::OFFICE_VALUE, QString::number(numericValue)); // C locale
    _attributes.append(Attributes::TABLE_STYLENAME, DefaultStyleNames::CURRENCY);
}

void TableCell::setDateValue(QDate dateValue)
{
    if(dateValue.isValid()) {
        _valueType = DATE;
        _content.clearText();
        _attributes.clear();
        _attributes.reserve(3);
        _attributes.append(Attributes::OFFICE_VALUETYPE, Values::DATE);
        _attributes.append(Attributes::OFFICE_DATEVALUE, dateValue.toString(Qt::ISODate));
        _attributes.append(Attributes::TABLE_STYLENAME, DefaultStyleNames::DATE);
    }
    else {
        setVoidValue();
    }
}

void TableCell::setFloatValue(double floatValue)
{
    _valueType = FLOAT;
    _content.clearText();
    _attributes.clear();
    _attributes.reserve(2);
    _attributes.append(Attributes::OFFICE_VALUETYPE, Values::FLOAT);
    _attributes.append(Attributes::OFFICE_VALUE, QString::number(floatValue)); // C locale
}

void TableCell::setPercentageValue(double percentageValue)
{
    _valueType = PERCENTAGE;
    _content.clearText();
    _attributes.clear();
    _attributes.reserve(3);
    _attributes.append(Attributes::OFFICE_VALUETYPE, Values::PERCENTAGE);
    _attributes.append(Attributes::OFFICE_VALUE, QString::number(percentageValue)); // C locale
    _attributes.append(Attributes::TABLE_STYLENAME, DefaultStyleNames::PERCENTAGE);
}

void TableCell::setStringValue(QString stringValue)
{
    _valueType = STRING;
    _content.setText(std::move(stringValue));
    _attributes.clear();
    _attributes.reserve(1);
    _attributes.append(Attributes::OFFICE_VALUETYPE, Values::STRING);
}

void TableCell::setVoidValue()
{
    _valueType = VOID;
    _content.clearText();
    _attributes.clear();
    _attributes.reserve(1);
    _attributes.append(Attributes::OFFICE_VALUETYPE, Values::VOID);
}

bool TableCell::deserializeFrom(QXmlStreamReader &xmlReader)
{
    if(xmlReader.isStartElement() && xmlReader.qualifiedName() == Elements::TABLE_TABLECELL) {
        _attributes = xmlReader.attributes();
        _valueType = stringToValueType(_attributes.value(Attributes::OFFICE_VALUETYPE));

        auto attributeIt = std::find_if(_attributes.begin(), _attributes.end(), [](const QXmlStreamAttribute &attribute) {
            return attribute.qualifiedName() == Attributes::TABLE_NUMBERCOLUMNSSPANNED;
        });
        if(attributeIt != _attributes.end()) {
            _numberColumnsSpanned = attributeIt->value().toULong();
            _attributes.erase(attributeIt);
        }

        attributeIt = std::find_if(_attributes.begin(), _attributes.end(), [](const QXmlStreamAttribute &attribute) {
            return attribute.qualifiedName() == Attributes::TABLE_NUMBERCOLUMNSSPANNED;
        });
        if(attributeIt != _attributes.end()) {
            _numberRowsSpanned = attributeIt->value().toULong();
            _attributes.erase(attributeIt);
        }

        while(!xmlReader.atEnd()) {
            xmlReader.readNext();

            _content.deserializeFrom(xmlReader);

            if(xmlReader.isEndElement() && xmlReader.qualifiedName() == Elements::TABLE_TABLECELL) {
                return true;
            }
        }
    }

    return false;
}

void TableCell::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    xmlWriter.writeStartElement(Elements::TABLE_TABLECELL);
    xmlWriter.writeAttributes(_attributes);
    if(_numberColumnsSpanned > 1 || _numberRowsSpanned > 1) {
        xmlWriter.writeAttribute(Attributes::TABLE_NUMBERCOLUMNSSPANNED, QString::number(_numberColumnsSpanned));
        xmlWriter.writeAttribute(Attributes::TABLE_NUMBERROWSSPANNED, QString::number(_numberRowsSpanned));
    }

    if(!_content.isEmpty()) {
        _content.serializeTo(xmlWriter);
    }

    xmlWriter.writeEndElement();
}

QString TableCell::valueTypeToString(ValueType valueType)
{
    switch(valueType) {
    case QtOdf::Table::TableCell::BOOLEAN: return Values::BOOLEAN;
    case QtOdf::Table::TableCell::CURRENCY: return Values::CURRENCY;
    case QtOdf::Table::TableCell::DATE: return Values::DATE;
    case QtOdf::Table::TableCell::FLOAT: return Values::FLOAT;
    case QtOdf::Table::TableCell::PERCENTAGE: return Values::PERCENTAGE;
    case QtOdf::Table::TableCell::STRING: return Values::STRING;
    case QtOdf::Table::TableCell::TIME: return Values::TIME;
    case QtOdf::Table::TableCell::VOID: return Values::VOID;
    default: return Values::VOID;
    }
}

TableCell::ValueType TableCell::stringToValueType(QStringView valueTypeAsString)
{
    if(valueTypeAsString == Values::BOOLEAN) return BOOLEAN;
    if(valueTypeAsString == Values::CURRENCY) return CURRENCY;
    if(valueTypeAsString == Values::DATE) return DATE;
    if(valueTypeAsString == Values::FLOAT) return FLOAT;
    if(valueTypeAsString == Values::PERCENTAGE) return PERCENTAGE;
    if(valueTypeAsString == Values::STRING) return STRING;
    if(valueTypeAsString == Values::TIME) return TIME;
    if(valueTypeAsString == Values::VOID) return VOID;
    return VOID;
}
