#include <QtCore/QDebug>
#include <QtOdf/Table/TableRow.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Table;

size_t TableRow::columnCount() const
{
    return std::accumulate(_tableCells.cbegin(), _tableCells.cend(), 0UL, [](size_t acc, const TableCell &tableCell) {
        return acc + tableCell.numberColumnsSpanned();
    });
}

TableCell *TableRow::cellAt(size_t column)
{
    size_t currentColumn = 0;
    size_t cellIndex = 0;

    while(cellIndex < _tableCells.size()) {
        auto &currentCell = _tableCells[cellIndex];
        size_t nextColumn = currentColumn + currentCell.numberColumnsSpanned();

        if(column < currentColumn) {
            return nullptr;
        }
        if(column >= currentColumn && column < nextColumn) {
            return &currentCell;
        }

        currentColumn = nextColumn;
        cellIndex++;
    }

    return nullptr;
}

const TableCell *TableRow::cellAt(size_t column) const
{
    size_t currentColumn = 0;
    size_t cellIndex = 0;

    while(cellIndex < _tableCells.size()) {
        const auto &currentCell = _tableCells[cellIndex];
        size_t nextColumn = currentColumn + currentCell.numberColumnsSpanned();

        if(column < currentColumn) {
            return nullptr;
        }
        if(column >= currentColumn && column < nextColumn) {
            return &currentCell;
        }

        currentColumn = nextColumn;
        cellIndex++;
    }

    return nullptr;
}

TableCell &TableRow::cell(size_t column)
{
    if(auto *cell = cellAt(column)) {
        return *cell;
    }
    else {
        const size_t columnCount = this->columnCount();
        if(column >= columnCount) {
            const size_t columnsToAdd = column - columnCount + 1;
            _tableCells.resize(_tableCells.size() + columnsToAdd);
        }
        Q_ASSERT(column < this->columnCount());

        return _tableCells.back();
    }
}

bool TableRow::deserializeFrom(QXmlStreamReader &xmlReader)
{
    _tableCells.clear();

    if(xmlReader.isStartElement() && xmlReader.qualifiedName() == Elements::TABLE_TABLEROW) {
        while(!xmlReader.atEnd()) {
            xmlReader.readNext();

            TableCell tableCell;
            if(tableCell.deserializeFrom(xmlReader)) {
                _tableCells.emplace_back(std::move(tableCell));
            }

            if(xmlReader.isEndElement() && xmlReader.qualifiedName() == Elements::TABLE_TABLEROW) {
                return true;
            }
        }
    }

    return false;
}

void TableRow::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    xmlWriter.writeStartElement(Elements::TABLE_TABLEROW);
    xmlWriter.writeAttribute(Attributes::TABLE_STYLENAME, DefaultStyleNames::TABLE_ROW);

    for(const TableCell &tableCell: _tableCells) {
        tableCell.serializeTo(xmlWriter);
    }

    xmlWriter.writeEndElement();
}
