#include <QtCore/QDebug>
#include <QtOdf/Table/Table.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Table;

Table::Table(QString tableName)
    : _tableName(std::move(tableName))
{
}

const QString &Table::name() const
{
    return _tableName;
}

size_t Table::rowCount() const
{
    return _tableRows.size();
}

size_t Table::columnCount() const
{
    return std::accumulate(_tableColumns.cbegin(), _tableColumns.cend(), 0UL, [](size_t acc, const TableColumn &tableColumn) {
        return acc + tableColumn.numberColumnsRepeated();
    });
}

TableCell *Table::cellAt(size_t row, size_t column)
{
    if(row < rowCount() && column < columnCount()) {
        return _tableRows[row].cellAt(column);
    }
    else {
        return nullptr;
    }
}

const TableCell *Table::cellAt(size_t row, size_t column) const
{
    if(row < rowCount() && column < columnCount()) {
        return _tableRows[row].cellAt(column);
    }
    else {
        return nullptr;
    }
}

TableCell &Table::cell(size_t row, size_t column)
{
    const size_t columnCount = this->columnCount();
    if(column >= columnCount) {
        const size_t columnsToAdd = column - columnCount + 1;
        _tableColumns.resize(_tableColumns.size() + columnsToAdd);
    }
    Q_ASSERT(column < this->columnCount());

    const size_t rowCount = this->rowCount();
    if(row >= rowCount) {
        const size_t rowsToAdd = row - rowCount + 1;
        _tableRows.resize(_tableRows.size() + rowsToAdd);
    }
    Q_ASSERT(row < this->rowCount());

    return _tableRows[row].cell(column);
}

bool Table::deserializeFrom(QXmlStreamReader &xmlReader)
{
    _tableColumns.clear();
    _tableRows.clear();

    if(xmlReader.isStartElement() && xmlReader.qualifiedName() == Elements::TABLE_TABLE) {
        const QXmlStreamAttributes attributes = xmlReader.attributes();
        _tableName = attributes.value(Attributes::TABLE_NAME).toString();

        while(!xmlReader.atEnd()) {
            xmlReader.readNext();

            TableColumn tableColumn;
            if(tableColumn.deserializeFrom(xmlReader)) {
                _tableColumns.emplace_back(std::move(tableColumn));
            }

            TableRow tableRow;
            if(tableRow.deserializeFrom(xmlReader)) {
                _tableRows.emplace_back(std::move(tableRow));
            }

            if(xmlReader.isEndElement() && xmlReader.qualifiedName() == Elements::TABLE_TABLE) {
                return true;
            }
        }
    }

    return false;
}

void Table::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    xmlWriter.writeStartElement(Elements::TABLE_TABLE);
    xmlWriter.writeAttribute(Attributes::TABLE_NAME, _tableName);

    for(const TableColumn &tableColumn: _tableColumns) {
        tableColumn.serializeTo(xmlWriter);
    }
    for(const TableRow &tableRow: _tableRows) {
        tableRow.serializeTo(xmlWriter);
    }

    xmlWriter.writeEndElement();
}
