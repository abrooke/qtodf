#include <QtOdf/Table/TableColumn.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Table;

TableColumn::TableColumn()
    : _numberColumnsRepeated(1)
{
}

size_t TableColumn::numberColumnsRepeated() const
{
    return _numberColumnsRepeated;
}

bool TableColumn::deserializeFrom(QXmlStreamReader &xmlReader)
{
    if(xmlReader.isStartElement() && xmlReader.qualifiedName() == Elements::TABLE_TABLECOLUMN) {
        const QXmlStreamAttributes attributes = xmlReader.attributes();
        if(attributes.hasAttribute(Attributes::TABLE_NUMBERCOLUMNSREPEATED)) {
            _numberColumnsRepeated = attributes.value(Attributes::TABLE_NUMBERCOLUMNSREPEATED).toULongLong();
        }

        while(!xmlReader.atEnd()) {
            xmlReader.readNext();

            if(xmlReader.isEndElement() && xmlReader.qualifiedName() == Elements::TABLE_TABLECOLUMN) {
                return true;
            }
        }
    }

    return false;
}

void TableColumn::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    xmlWriter.writeStartElement(Elements::TABLE_TABLECOLUMN);
    xmlWriter.writeAttribute(Attributes::TABLE_STYLENAME, DefaultStyleNames::TABLE_COLUMN);
    if(_numberColumnsRepeated > 1) {
        xmlWriter.writeAttribute(Attributes::TABLE_NUMBERCOLUMNSREPEATED, QString::number(_numberColumnsRepeated));
    }

    xmlWriter.writeEndElement();
}
