#include <QtCore/QDebug>
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
#include <QtGui/private/qzipreader_p.h>
#include <QtGui/private/qzipwriter_p.h>
#else
#include <QtCore/private/qzipreader_p.h>
#include <QtCore/private/qzipwriter_p.h>
#endif
#include <QtOdf/Constants.h>
#include <QtOdf/OpenDocumentFile.h>

using namespace QtOdf;

const QString OpenDocumentFile::FileNames::MIMETYPE = QStringLiteral("mimetype");
const QString OpenDocumentFile::FileNames::MANIFEST_RDF = QStringLiteral("manifest.rdf");
const QString OpenDocumentFile::FileNames::META_XML = QStringLiteral("meta.xml");
const QString OpenDocumentFile::FileNames::STYLES_XML = QStringLiteral("styles.xml");
const QString OpenDocumentFile::FileNames::CONTENT_XML = QStringLiteral("content.xml");
const QString OpenDocumentFile::FileNames::SETTINGS_XML = QStringLiteral("settings.xml");
const QString OpenDocumentFile::FileNames::META_INF_MANIFEST_XML = QStringLiteral("META-INF/manifest.xml");

OpenDocumentFile::OpenDocumentFile()
{
}

OpenDocumentFile::~OpenDocumentFile()
{
}

OpenDocumentFile::Error OpenDocumentFile::load(const QString &fileName, QIODevice::OpenMode mode)
{
    QFile file(fileName);
    const bool openResult = file.open(mode);
    const QFileDevice::FileError error = file.error();

    if (openResult && error == QFile::NoError) {
        return load(file);
    }
    else {
        qWarning().nospace() << "Cannot open file " << fileName << ": " << qUtf8Printable(file.errorString());

        if(error == QFile::ReadError)
            return Error::FileReadError;
        else if(error == QFile::OpenError)
            return Error::FileOpenError;
        else if(error == QFile::PermissionsError)
            return Error::FilePermissionsError;
        else
            return Error::FileUnkownError;
    }
}

OpenDocumentFile::Error OpenDocumentFile::load(QIODevice &device)
{
    QZipReader zipReader(&device);

    if(!zipReader.isReadable()) {
        return Error::DeviceNotReadable;
    }

    // Deserialize content
    const QByteArray contentXml = zipReader.fileData(FileNames::CONTENT_XML);
    QXmlStreamReader contentXmlReader(contentXml);
    bool documentContentElementFound = false;
    while(!contentXmlReader.atEnd()) {
        contentXmlReader.readNext();

        if(!documentContentElementFound) {
            if(contentXmlReader.isStartElement() && contentXmlReader.qualifiedName() == Elements::OFFICE_DOCUMENTCONTENT) {
                documentContentElementFound = true;
            }
        }
        else {
            _automaticStyles.deserializeFrom(contentXmlReader);
            if(contentXmlReader.isStartElement() && contentXmlReader.qualifiedName() == Elements::OFFICE_BODY) {
                if(deserializeOfficeBodyContentsFrom(contentXmlReader)) {
                    return Error::NoError;
                }
                else {
                    return Error::DeserializationError;
                }
            }
            if(contentXmlReader.isEndElement() && contentXmlReader.qualifiedName() == Elements::OFFICE_BODY) {
                break;
            }
        }
    }

    return Error::DeserializationError;
}

OpenDocumentFile::Error OpenDocumentFile::save(const QString &fileName, QIODevice::OpenMode mode)
{
    QFile file(fileName);
    const bool openResult = file.open(mode);
    const QFileDevice::FileError error = file.error();

    if (openResult && error == QFile::NoError) {
        return save(file);
    }
    else {
        qWarning().nospace() << "Cannot open file " << fileName << ": " << qUtf8Printable(file.errorString());

        if (file.error() == QFile::WriteError)
            return Error::FileWriteError;
        else if (file.error() == QFile::OpenError)
            return Error::FileOpenError;
        else if (file.error() == QFile::PermissionsError)
            return Error::FilePermissionsError;
        else
            return Error::FileUnkownError;
    }
}

OpenDocumentFile::Error OpenDocumentFile::save(QIODevice &device)
{
    QZipWriter zipWriter(&device);
    // This will auto-magically not compress the "mimetype" file because it is under 64 bytes, and AutoCompress skips compression of files < 64 bytes!
    zipWriter.setCompressionPolicy(QZipWriter::AutoCompress);

    if(!zipWriter.isWritable()) {
        return Error::DeviceNotWritable;
    }

    const QString mimeType = this->mimeType();
    if(mimeType.isEmpty() || mimeType.contains(u'\n')) {
        return Error::BadMimetype;
    }
    zipWriter.addFile(FileNames::MIMETYPE, mimeType.toLatin1());

    QByteArray metaInfManifest;
    QXmlStreamWriter metaInfManifestXmlWriter(&metaInfManifest);
    metaInfManifestXmlWriter.setAutoFormatting(true);
    metaInfManifestXmlWriter.writeStartDocument(QStringLiteral("1.0"));
    metaInfManifestXmlWriter.writeStartElement(Elements::MANIFEST_MANIFEST);
    metaInfManifestXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:manifest:1.0"), QStringLiteral("manifest"));
    metaInfManifestXmlWriter.writeAttribute(Attributes::MANIFEST_VERSION, QStringLiteral("1.3"));
    generateMetaInfManifestEntry(metaInfManifestXmlWriter, QStringLiteral("/"), mimeType);

    // Serialize content
    QByteArray contentXml;
    QXmlStreamWriter contentXmlWriter(&contentXml);
    contentXmlWriter.setAutoFormatting(true);
    contentXmlWriter.writeStartDocument(QStringLiteral("1.0"));
    contentXmlWriter.writeStartElement(Elements::OFFICE_DOCUMENTCONTENT);
    contentXmlWriter.writeNamespace(QStringLiteral("http://openoffice.org/2004/calc"), QStringLiteral("oooc"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://openoffice.org/2004/office"), QStringLiteral("ooo"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://openoffice.org/2004/writer"), QStringLiteral("ooow"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://openoffice.org/2005/report"), QStringLiteral("rpt"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://openoffice.org/2009/table"), QStringLiteral("tableooo"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://openoffice.org/2010/draw"), QStringLiteral("drawooo"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://purl.org/dc/elements/1.1/"), QStringLiteral("dc"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://www.w3.org/1998/Math/MathML"), QStringLiteral("math"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://www.w3.org/1999/xhtml"), QStringLiteral("xlink"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://www.w3.org/1999/xlink"), QStringLiteral("xhtml"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://www.w3.org/2001/xml-events"), QStringLiteral("dom"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://www.w3.org/2001/XMLSchema"), QStringLiteral("xsd"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://www.w3.org/2001/XMLSchema-instance"), QStringLiteral("xsi"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://www.w3.org/2002/xforms"), QStringLiteral("xforms"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://www.w3.org/2003/g/data-view#"), QStringLiteral("grddl"));
    contentXmlWriter.writeNamespace(QStringLiteral("http://www.w3.org/TR/css3-text/"), QStringLiteral("css3t"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:chart:1.0"), QStringLiteral("chart"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"), QStringLiteral("number"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0"), QStringLiteral("dr3d"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"), QStringLiteral("draw"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"), QStringLiteral("fo"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:form:1.0"), QStringLiteral("form"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:meta:1.0"), QStringLiteral("meta"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:of:1.2"), QStringLiteral("of"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:office:1.0"), QStringLiteral("office"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"), QStringLiteral("presentation"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:script:1.0"), QStringLiteral("script"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:style:1.0"), QStringLiteral("style"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"), QStringLiteral("svg"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:table:1.0"), QStringLiteral("table"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:oasis:names:tc:opendocument:xmlns:text:1.0"), QStringLiteral("text"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0"), QStringLiteral("field"));
    contentXmlWriter.writeNamespace(QStringLiteral("urn:openoffice:names:experimental:ooxml-odf-interop:xmlns:form:1.0"), QStringLiteral("formx"));
    contentXmlWriter.writeAttribute(Attributes::OFFICE_VERSION, QStringLiteral("1.3"));
    _automaticStyles.serializeTo(contentXmlWriter);
    contentXmlWriter.writeStartElement(Elements::OFFICE_BODY);
    serializeOfficeBodyContentsTo(contentXmlWriter);
    contentXmlWriter.writeEndElement();
    contentXmlWriter.writeEndElement();
    contentXmlWriter.writeEndDocument();
    zipWriter.addFile(FileNames::CONTENT_XML, contentXml);
    generateMetaInfManifestEntry(metaInfManifestXmlWriter, FileNames::CONTENT_XML, MimeTypes::TEXT_XML);

    metaInfManifestXmlWriter.writeEndDocument();
    zipWriter.addFile(FileNames::META_INF_MANIFEST_XML, metaInfManifest);

    return Error::NoError;
}

Office::AutomaticStyles &OpenDocumentFile::automaticStyles()
{
    return _automaticStyles;
}

void OpenDocumentFile::generateMetaInfManifestEntry(QXmlStreamWriter &xmlStreamWriter, const QString &fullPath, const QString &mediaType)
{
    xmlStreamWriter.writeStartElement(Elements::MANIFEST_FILEENTRY);

    xmlStreamWriter.writeAttribute(Attributes::MANIFEST_FULLPATH, fullPath);
    xmlStreamWriter.writeAttribute(Attributes::MANIFEST_MEDIATYPE, mediaType);

    xmlStreamWriter.writeEndElement();
}
