#include <QtOdf/Number/TimeStyle.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Number;

bool TimeStyle::deserializeFrom(QXmlStreamReader &xmlReader)
{
    Q_UNUSED(xmlReader)

    return false;
}

void TimeStyle::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    // TODO implement customizable styles
    // this is only mock-up, hard-wired implementation

    xmlWriter.writeStartElement(Elements::NUMBER_TIMESTYLE);
    xmlWriter.writeAttribute(Attributes::STYLE_NAME, styleName());

    xmlWriter.writeStartElement(Elements::NUMBER_HOURS);
    xmlWriter.writeAttribute(Attributes::NUMBER_STYLE, Values::LONG);
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement(Elements::NUMBER_TEXT);
    xmlWriter.writeCharacters(QStringLiteral(":"));
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement(Elements::NUMBER_MINUTES);
    xmlWriter.writeAttribute(Attributes::NUMBER_STYLE, Values::LONG);
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement(Elements::NUMBER_TEXT);
    xmlWriter.writeCharacters(QStringLiteral(":"));
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement(Elements::NUMBER_SECONDS);
    xmlWriter.writeAttribute(Attributes::NUMBER_STYLE, Values::LONG);
    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
}
