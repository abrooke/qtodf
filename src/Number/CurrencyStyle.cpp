#include <QtCore/QLocale>
#include <QtOdf/Number/CurrencyStyle.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Number;

const QLocale &CurrencyStyle::locale() const
{
    return _locale;
}

void CurrencyStyle::setLocale(QLocale locale)
{
    _locale = std::move(locale);
}

const QString &CurrencyStyle::currencySymbol() const
{
    return _currencySymbol;
}

void CurrencyStyle::setCurrencySymbol(QString newCurrencySymbol)
{
    _currencySymbol = std::move(newCurrencySymbol);
}

void CurrencyStyle::clearCurrencySymbol()
{
    _currencySymbol.clear();
}

bool CurrencyStyle::deserializeFrom(QXmlStreamReader &xmlReader)
{
    Q_UNUSED(xmlReader)

    return false;
}

void CurrencyStyle::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    // TODO implement customizable styles
    // this is only mock-up, hard-wired implementation

    xmlWriter.writeStartElement(Elements::NUMBER_CURRENCYSTYLE);
    xmlWriter.writeAttribute(Attributes::STYLE_NAME, styleName());
    xmlWriter.writeAttribute(Attributes::NUMBER_AUTOMATICORDER, Values::TRUE);

    xmlWriter.writeStartElement(Elements::NUMBER_NUMBER);
    xmlWriter.writeAttribute(Attributes::NUMBER_DECIMALPLACES, QString::number(2));
    xmlWriter.writeAttribute(Attributes::NUMBER_MINDECIMALPLACES, QString::number(2));
    xmlWriter.writeAttribute(Attributes::NUMBER_MININTEGERDIGITS, QString::number(1));
    xmlWriter.writeAttribute(Attributes::NUMBER_GROUPING, Values::TRUE);
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement(Elements::NUMBER_TEXT);
    xmlWriter.writeCharacters(QStringLiteral("\u00A0")); // nbsp
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement(Elements::NUMBER_CURRENCYSYMBOL);
    const QStringList languageAndCountry = _locale.name().split(u'_');
    if(languageAndCountry.size() == 2) {
        xmlWriter.writeAttribute(Attributes::NUMBER_LANGUAGE, languageAndCountry.at(0));
        xmlWriter.writeAttribute(Attributes::NUMBER_COUNTRY, languageAndCountry.at(1));
    }
    if(!_currencySymbol.isEmpty()) {
        xmlWriter.writeCharacters(_currencySymbol);
    }
    else {
        xmlWriter.writeCharacters(_locale.currencySymbol());
    }
    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
}
