#include <QtOdf/Number/PercentageStyle.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Number;

bool PercentageStyle::deserializeFrom(QXmlStreamReader &xmlReader)
{
    Q_UNUSED(xmlReader)

    return false;
}

void PercentageStyle::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    // TODO implement customizable styles
    // this is only mock-up, hard-wired implementation

    xmlWriter.writeStartElement(Elements::NUMBER_PERCENTAGESTYLE);
    xmlWriter.writeAttribute(Attributes::STYLE_NAME, styleName());

    xmlWriter.writeStartElement(Elements::NUMBER_NUMBER);
    xmlWriter.writeAttribute(Attributes::NUMBER_DECIMALPLACES, QString::number(2));
    xmlWriter.writeAttribute(Attributes::NUMBER_MINDECIMALPLACES, QString::number(2));
    xmlWriter.writeAttribute(Attributes::NUMBER_MININTEGERDIGITS, QString::number(1));
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement(Elements::NUMBER_TEXT);
    xmlWriter.writeCharacters(QStringLiteral("\u00A0%")); // nbsp%
    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
}
