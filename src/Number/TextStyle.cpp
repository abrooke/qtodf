#include <QtOdf/Number/TextStyle.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Number;

bool TextStyle::deserializeFrom(QXmlStreamReader &xmlReader)
{
    Q_UNUSED(xmlReader)

    return false;
}

void TextStyle::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    // TODO implement customizable styles
    // this is only mock-up, hard-wired implementation

    xmlWriter.writeStartElement(Elements::NUMBER_TEXTSTYLE);
    xmlWriter.writeAttribute(Attributes::STYLE_NAME, styleName());

    xmlWriter.writeEndElement();
}
