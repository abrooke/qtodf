#include <QtOdf/Number/DateStyle.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Number;

bool DateStyle::deserializeFrom(QXmlStreamReader &xmlReader)
{
    Q_UNUSED(xmlReader)

    return false;
}

void DateStyle::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    // TODO implement customizable styles
    // this is only mock-up, hard-wired implementation

    xmlWriter.writeStartElement(Elements::NUMBER_DATESTYLE);
    xmlWriter.writeAttribute(Attributes::NUMBER_AUTOMATICORDER, Values::TRUE);
    xmlWriter.writeAttribute(Attributes::STYLE_NAME, styleName());

    xmlWriter.writeStartElement(Elements::NUMBER_DAY);
    xmlWriter.writeAttribute(Attributes::NUMBER_STYLE, Values::LONG);
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement(Elements::NUMBER_TEXT);
    xmlWriter.writeCharacters(QStringLiteral("/"));
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement(Elements::NUMBER_MONTH);
    xmlWriter.writeAttribute(Attributes::NUMBER_STYLE, Values::LONG);
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement(Elements::NUMBER_TEXT);
    xmlWriter.writeCharacters(QStringLiteral("/"));
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement(Elements::NUMBER_YEAR);
    xmlWriter.writeAttribute(Attributes::NUMBER_STYLE, Values::LONG);
    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
}
