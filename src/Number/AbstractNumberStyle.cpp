#include <QtOdf/Number/AbstractNumberStyle.h>

using namespace QtOdf::Number;

const QString &AbstractNumberStyle::styleName() const
{
    return _styleName;
}

void AbstractNumberStyle::setStyleName(QString styleName)
{
    _styleName = std::move(styleName);
}
