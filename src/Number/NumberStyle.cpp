#include <QtOdf/Number/NumberStyle.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Number;

bool NumberStyle::deserializeFrom(QXmlStreamReader &xmlReader)
{
    Q_UNUSED(xmlReader)

    return false;
}

void NumberStyle::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    // TODO implement customizable styles
    // this is only mock-up, hard-wired implementation

    xmlWriter.writeStartElement(Elements::NUMBER_NUMBERSTYLE);
    xmlWriter.writeAttribute(Attributes::STYLE_NAME, styleName());

    xmlWriter.writeEndElement();
}
