#include <QtOdf/Text/P.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Text;

bool P::isEmpty() const
{
    return _text.isEmpty();
}

const QString &P::text() const
{
    return _text;
}

void P::setText(QString text)
{
    _text = std::move(text);
}

void P::clearText()
{
    _text.clear();
}

bool P::deserializeFrom(QXmlStreamReader &xmlReader)
{
    if(xmlReader.isStartElement() && xmlReader.qualifiedName() == Elements::TEXT_P) {
        _text = xmlReader.readElementText(QXmlStreamReader::SkipChildElements);

        if(xmlReader.isEndElement() && xmlReader.qualifiedName() == Elements::TEXT_P) {
            return true;
        }
    }

    return false;
}

void P::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    xmlWriter.writeStartElement(Elements::TEXT_P);

    xmlWriter.writeCharacters(_text);

    xmlWriter.writeEndElement();
}
