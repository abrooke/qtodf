#include <QtOdf/Office/AutomaticStyles.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Office;

AutomaticStyles::AutomaticStyles()
{
    _defaultTableStyle.setStyleName(DefaultStyleNames::TABLE);
    _defaultTableCellStyle.setStyleName(DefaultStyleNames::TABLE_CELL);
    _defaultTableColumnStyle.setStyleName(DefaultStyleNames::TABLE_COLUMN);
    _defaultTableRowStyle.setStyleName(DefaultStyleNames::TABLE_ROW);

    _defaultBooleanStyle.setStyleName(QStringLiteral("default-boolean-style"));
    _defaultCurrencyStyle.setStyleName(QStringLiteral("default-currency-style"));
    _defaultDateStyle.setStyleName(QStringLiteral("default-date-style"));
    _defaultPercentageStyle.setStyleName(QStringLiteral("default-percentage-style"));
    _defaultTimeStyle.setStyleName(QStringLiteral("default-time-style"));
}

AutomaticStyles::~AutomaticStyles()
{
}

QString AutomaticStyles::elementName() const
{
    return Elements::OFFICE_AUTOMATICSTYLES;
}

QtOdf::Style::TableStyle &AutomaticStyles::defaultTableStyle()
{
    return _defaultTableStyle;
}

QtOdf::Style::TableCellStyle &AutomaticStyles::defaultTableCellStyle()
{
    return _defaultTableCellStyle;
}

QtOdf::Style::TableColumnStyle &AutomaticStyles::defaultTableColumnStyle()
{
    return _defaultTableColumnStyle;
}

QtOdf::Style::TableRowStyle &AutomaticStyles::defaultTableRowStyle()
{
    return _defaultTableRowStyle;
}

QtOdf::Number::BooleanStyle &AutomaticStyles::defaultBooleanStyle()
{
    return _defaultBooleanStyle;
}

QtOdf::Number::CurrencyStyle &AutomaticStyles::defaultCurrencyStyle()
{
    return _defaultCurrencyStyle;
}

QtOdf::Number::DateStyle &AutomaticStyles::defaultDateStyle()
{
    return _defaultDateStyle;
}

QtOdf::Number::PercentageStyle &AutomaticStyles::defaultPercentageStyle()
{
    return _defaultPercentageStyle;
}

QtOdf::Number::TimeStyle &AutomaticStyles::defaultTimeStyle()
{
    return _defaultTimeStyle;
}

bool AutomaticStyles::deserializeFrom(QXmlStreamReader &xmlReader)
{
    if(xmlReader.isStartElement() && xmlReader.qualifiedName() == Elements::OFFICE_AUTOMATICSTYLES) {
        // TODO

        while(!xmlReader.atEnd()) {
            xmlReader.readNext();

            if(xmlReader.isEndElement() && xmlReader.qualifiedName() == Elements::OFFICE_AUTOMATICSTYLES) {
                return true;
            }
        }
    }

    return false;
}

void AutomaticStyles::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    xmlWriter.writeStartElement(Elements::OFFICE_AUTOMATICSTYLES);

    // TODO implement customizable styles
    // this is only mock-up, hard-wired implementation

    _defaultTableStyle.serializeTo(xmlWriter);
    _defaultTableCellStyle.serializeTo(xmlWriter);
    _defaultTableColumnStyle.serializeTo(xmlWriter);
    _defaultTableRowStyle.serializeTo(xmlWriter);

    {
        _defaultBooleanStyle.serializeTo(xmlWriter);
        Style::TableCellStyle booleanTableCellStyle(DefaultStyleNames::BOOLEAN);
        booleanTableCellStyle.setDataStyleName(_defaultBooleanStyle.styleName());
        booleanTableCellStyle.serializeTo(xmlWriter);
    }

    {
        _defaultCurrencyStyle.serializeTo(xmlWriter);
        Style::TableCellStyle currencyTableCellStyle(DefaultStyleNames::CURRENCY);
        currencyTableCellStyle.setDataStyleName(_defaultCurrencyStyle.styleName());
        currencyTableCellStyle.serializeTo(xmlWriter);
    }

    {
        _defaultDateStyle.serializeTo(xmlWriter);
        Style::TableCellStyle dateTableCellStyle(DefaultStyleNames::DATE);
        dateTableCellStyle.setDataStyleName(_defaultDateStyle.styleName());
        dateTableCellStyle.serializeTo(xmlWriter);
    }

    {
        _defaultPercentageStyle.serializeTo(xmlWriter);
        Style::TableCellStyle percentageTableCellStyle(DefaultStyleNames::PERCENTAGE);
        percentageTableCellStyle.setDataStyleName(_defaultPercentageStyle.styleName());
        percentageTableCellStyle.serializeTo(xmlWriter);
    }

    {
        _defaultTimeStyle.serializeTo(xmlWriter);
        Style::TableCellStyle timeTableCellStyle(DefaultStyleNames::TIME);
        timeTableCellStyle.setDataStyleName(_defaultTimeStyle.styleName());
        timeTableCellStyle.serializeTo(xmlWriter);
    }

    xmlWriter.writeEndElement();
}
