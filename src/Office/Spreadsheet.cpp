#include <QtCore/QDebug>
#include <QtOdf/Office/Spreadsheet.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Office;

Spreadsheet::Spreadsheet()
{

}

QtOdf::Table::Table *Spreadsheet::tableAt(size_t tableIndex)
{
    if(tableIndex < _tables.size()) {
        return &_tables[tableIndex];
    }
    else {
        return nullptr;
    }
}

const QtOdf::Table::Table *Spreadsheet::tableAt(size_t tableIndex) const
{
    if(tableIndex < _tables.size()) {
        return &_tables[tableIndex];
    }
    else {
        return nullptr;
    }
}

QtOdf::Table::Table *Spreadsheet::tableAt(QStringView tableName)
{
    for(Table::Table &table: _tables) {
        if(table.name() == tableName) {
            return &table;
        }
    };

    return nullptr;
}

const QtOdf::Table::Table *Spreadsheet::tableAt(QStringView tableName) const
{
    for(const Table::Table &table: _tables) {
        if(table.name() == tableName) {
            return &table;
        }
    };

    return nullptr;
}

QtOdf::Table::Table &Spreadsheet::table(QStringView tableName)
{
    for(Table::Table &table: _tables) {
        if(table.name() == tableName) {
            return table;
        }
    };

    return _tables.emplace_back(tableName.toString());
}

QtOdf::Table::Table &Spreadsheet::table(QString tableName)
{
    for(Table::Table &table: _tables) {
        if(table.name() == tableName) {
            return table;
        }
    };

    return _tables.emplace_back(std::move(tableName));
}

bool Spreadsheet::deserializeFrom(QXmlStreamReader &xmlReader)
{
    _tables.clear();

    if(xmlReader.isStartElement() && xmlReader.qualifiedName() == Elements::OFFICE_SPREADSHEET) {
        while(!xmlReader.atEnd()) {
            xmlReader.readNext();

            Table::Table table;
            if(table.deserializeFrom(xmlReader)) {
                _tables.emplace_back(std::move(table));
            }

            if(xmlReader.isEndElement() && xmlReader.qualifiedName() == Elements::OFFICE_SPREADSHEET) {
                return true;
            }
        }
    }

    return false;
}

void Spreadsheet::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    xmlWriter.writeStartElement(Elements::OFFICE_SPREADSHEET);

    for(const Table::Table &table: _tables) {
        table.serializeTo(xmlWriter);
    }

    xmlWriter.writeEndElement();
}
