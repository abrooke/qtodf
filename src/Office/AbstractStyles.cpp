#include <QtOdf/Number/BooleanStyle.h>
#include <QtOdf/Number/CurrencyStyle.h>
#include <QtOdf/Number/DateStyle.h>
#include <QtOdf/Number/PercentageStyle.h>
#include <QtOdf/Number/TimeStyle.h>
#include <QtOdf/Office/AbstractStyles.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Office;

AbstractStyles::~AbstractStyles()
{
}
