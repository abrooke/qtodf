#include <QtOdf/Office/Styles.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Office;

QString Styles::elementName() const
{
    return Elements::OFFICE_STYLES;
}

bool Styles::deserializeFrom(QXmlStreamReader &xmlReader)
{
    if(xmlReader.isStartElement() && xmlReader.qualifiedName() == Elements::OFFICE_STYLES) {
        // TODO

        while(!xmlReader.atEnd()) {
            xmlReader.readNext();

            if(xmlReader.isEndElement() && xmlReader.qualifiedName() == Elements::OFFICE_STYLES) {
                return true;
            }
        }
    }

    return false;
}

void Styles::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    xmlWriter.writeStartElement(Elements::OFFICE_STYLES);

    // TODO

    xmlWriter.writeEndElement();
}
