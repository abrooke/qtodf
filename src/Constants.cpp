#include <QtOdf/Constants.h>

using namespace QtOdf;

// Attributes

const QString Attributes::MANIFEST_FULLPATH = QStringLiteral("manifest:full-path");
const QString Attributes::MANIFEST_MEDIATYPE = QStringLiteral("manifest:media-type");
const QString Attributes::MANIFEST_VERSION = QStringLiteral("manifest:version");

const QString Attributes::NUMBER_AUTOMATICORDER = QStringLiteral("number:automatic-order");
const QString Attributes::NUMBER_COUNTRY = QStringLiteral("number:country");
const QString Attributes::NUMBER_DECIMALPLACES = QStringLiteral("number:decimal-places");
const QString Attributes::NUMBER_GROUPING = QStringLiteral("number:grouping");
const QString Attributes::NUMBER_LANGUAGE = QStringLiteral("number:language");
const QString Attributes::NUMBER_MINDECIMALPLACES = QStringLiteral("number:min-decimal-places");
const QString Attributes::NUMBER_MININTEGERDIGITS = QStringLiteral("number:min-integer-digits");
const QString Attributes::NUMBER_STYLE = QStringLiteral("number:style");

const QString Attributes::OFFICE_BOOLEANVALUE = QStringLiteral("office:boolean-value");
const QString Attributes::OFFICE_CURRENCY = QStringLiteral("office:currency");
const QString Attributes::OFFICE_DATEVALUE = QStringLiteral("office:date-value");
const QString Attributes::OFFICE_VALUE = QStringLiteral("office:value");
const QString Attributes::OFFICE_VALUETYPE = QStringLiteral("office:value-type");
const QString Attributes::OFFICE_VERSION = QStringLiteral("office:version");

const QString Attributes::STYLE_DATASTYLENAME = QStringLiteral("style:data-style-name");
const QString Attributes::STYLE_FAMILY = QStringLiteral("style:family");
const QString Attributes::STYLE_NAME = QStringLiteral("style:name");
const QString Attributes::STYLE_USEOPTIMALCOLUMNWIDTH = QStringLiteral("style:use-optimal-column-width");

const QString Attributes::TABLE_NAME = QStringLiteral("table:name");
const QString Attributes::TABLE_NUMBERCOLUMNSREPEATED = QStringLiteral("table:number-columns-repeated");
const QString Attributes::TABLE_NUMBERCOLUMNSSPANNED = QStringLiteral("table:number-columns-spanned");
const QString Attributes::TABLE_NUMBERROWSSPANNED = QStringLiteral("table:number-rows-spanned");
const QString Attributes::TABLE_STYLENAME = QStringLiteral("table:style-name");

// DefaultStyleNames

const QString DefaultStyleNames::TABLE = QStringLiteral("ta-default");
const QString DefaultStyleNames::TABLE_CELL = QStringLiteral("ce-default");
const QString DefaultStyleNames::TABLE_COLUMN = QStringLiteral("co-default");
const QString DefaultStyleNames::TABLE_ROW = QStringLiteral("ro-default");

const QString DefaultStyleNames::BOOLEAN = QStringLiteral("ce-boolean");
const QString DefaultStyleNames::CURRENCY = QStringLiteral("ce-currency");
const QString DefaultStyleNames::DATE = QStringLiteral("ce-date");
const QString DefaultStyleNames::PERCENTAGE = QStringLiteral("ce-percentage");
const QString DefaultStyleNames::TIME = QStringLiteral("ce-time");

// Elements

const QString Elements::MANIFEST_MANIFEST = QStringLiteral("manifest:manifest");
const QString Elements::MANIFEST_FILEENTRY = QStringLiteral("manifest:file-entry");

const QString Elements::NUMBER_BOOLEAN = QStringLiteral("number:boolean");
const QString Elements::NUMBER_BOOLEANSTYLE = QStringLiteral("number:boolean-style");
const QString Elements::NUMBER_CURRENCYSTYLE = QStringLiteral("number:currency-style");
const QString Elements::NUMBER_CURRENCYSYMBOL = QStringLiteral("number:currency-symbol");
const QString Elements::NUMBER_DATESTYLE = QStringLiteral("number:date-style");
const QString Elements::NUMBER_DAY = QStringLiteral("number:day");
const QString Elements::NUMBER_HOURS = QStringLiteral("number:hours");
const QString Elements::NUMBER_MINUTES = QStringLiteral("number:minutes");
const QString Elements::NUMBER_MONTH = QStringLiteral("number:month");
const QString Elements::NUMBER_NUMBER = QStringLiteral("number:number");
const QString Elements::NUMBER_NUMBERSTYLE = QStringLiteral("number:number-style");
const QString Elements::NUMBER_PERCENTAGESTYLE = QStringLiteral("number:percentage-style");
const QString Elements::NUMBER_SECONDS = QStringLiteral("number:seconds");
const QString Elements::NUMBER_TEXT = QStringLiteral("number:text");
const QString Elements::NUMBER_TEXTSTYLE = QStringLiteral("number:text-style");
const QString Elements::NUMBER_TIMESTYLE = QStringLiteral("number:time-style");
const QString Elements::NUMBER_YEAR = QStringLiteral("number:year");

const QString Elements::OFFICE_AUTOMATICSTYLES = QStringLiteral("office:automatic-styles");
const QString Elements::OFFICE_BODY = QStringLiteral("office:body");
const QString Elements::OFFICE_DOCUMENTCONTENT = QStringLiteral("office:document-content");
const QString Elements::OFFICE_FONTFACEDECLS = QStringLiteral("office:font-face-decls");
const QString Elements::OFFICE_SCRIPTS = QStringLiteral("office:scripts");
const QString Elements::OFFICE_SPREADSHEET = QStringLiteral("office:spreadsheet");
const QString Elements::OFFICE_STYLES = QStringLiteral("office:styles");
const QString Elements::OFFICE_TEXT = QStringLiteral("office:text");

const QString Elements::STYLE_STYLE = QStringLiteral("style:style");
const QString Elements::STYLE_TABLEPROPERTIES = QStringLiteral("style:table-properties");
const QString Elements::STYLE_TABLECELLPROPERTIES = QStringLiteral("style:table-cell-properties");
const QString Elements::STYLE_TABLECOLUMNPROPERTIES = QStringLiteral("style:table-column-properties");
const QString Elements::STYLE_TABLEROWPROPERTIES = QStringLiteral("style:table-row-properties");
const QString Elements::STYLE_TEXTPROPERTIES = QStringLiteral("style:text-properties");

const QString Elements::TABLE_TABLE = QStringLiteral("table:table");
const QString Elements::TABLE_TABLECOLUMN = QStringLiteral("table:table-column");
const QString Elements::TABLE_TABLEROW = QStringLiteral("table:table-row");
const QString Elements::TABLE_TABLECELL = QStringLiteral("table:table-cell");

const QString Elements::TEXT_P = QStringLiteral("text:p");

// MimeTypes

const QString MimeTypes::OD_SPREADSHEET = QStringLiteral("application/vnd.oasis.opendocument.spreadsheet");
const QString MimeTypes::OD_TEXT = QStringLiteral("application/vnd.oasis.opendocument.text");
const QString MimeTypes::RDFXML = QStringLiteral("application/rdf+xml");
const QString MimeTypes::TEXT_XML = QStringLiteral("text/xml");

// Values

const QString Values::BOOLEAN = QStringLiteral("boolean");
const QString Values::CURRENCY = QStringLiteral("currency");
const QString Values::DATE = QStringLiteral("date");
const QString Values::FLOAT = QStringLiteral("float");
const QString Values::PERCENTAGE = QStringLiteral("percentage");
const QString Values::STRING = QStringLiteral("string");
const QString Values::TIME = QStringLiteral("time");
const QString Values::VOID = QStringLiteral("void");

const QString Values::TRUE = QStringLiteral("true");
const QString Values::FALSE = QStringLiteral("false");

const QString Values::CHART = QStringLiteral("chart");
const QString Values::DRAWING_PAGE = QStringLiteral("drawing-page");
const QString Values::GRAPHIC = QStringLiteral("graphic");
const QString Values::PARAGRAPH = QStringLiteral("paragraph");
const QString Values::PRESENTATION = QStringLiteral("presentation");
const QString Values::RUBY = QStringLiteral("ruby");
const QString Values::TABLE = QStringLiteral("table");
const QString Values::TABLE_CELL = QStringLiteral("table-cell");
const QString Values::TABLE_COLUMN = QStringLiteral("table-column");
const QString Values::TABLE_ROW = QStringLiteral("table-row");
const QString Values::TEXT = QStringLiteral("text");

const QString Values::LONG = QStringLiteral("long");
const QString Values::SHORT = QStringLiteral("short");
