#include <QtOdf/Style/AbstractStyle.h>

using namespace QtOdf::Style;

AbstractStyle::AbstractStyle(QString styleName)
    : _styleName(std::move(styleName))
{
}

AbstractStyle::~AbstractStyle()
{
}

const QString &AbstractStyle::styleName() const
{
    return _styleName;
}

void AbstractStyle::setStyleName(QString styleName)
{
    _styleName = std::move(styleName);
}
