#include <QtOdf/Style/TableColumnStyle.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Style;

TableColumnStyle::TableColumnStyle()
    : _useOptimalColumnWidth(false)
{
}

TableColumnStyle::~TableColumnStyle()
{
}

QString TableColumnStyle::styleFamily() const
{
    return Values::TABLE_COLUMN;
}

bool TableColumnStyle::useOptimalColumnWidth() const
{
    return _useOptimalColumnWidth;
}

void TableColumnStyle::setUseOptimalColumnWidth(bool useOptimalColumnWidth)
{
    _useOptimalColumnWidth = useOptimalColumnWidth;
}

bool TableColumnStyle::deserializeFrom(QXmlStreamReader &xmlReader)
{
    Q_UNUSED(xmlReader)

    return false;
}

void TableColumnStyle::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    xmlWriter.writeStartElement(Elements::STYLE_STYLE);

    xmlWriter.writeAttribute(Attributes::STYLE_NAME, styleName());
    xmlWriter.writeAttribute(Attributes::STYLE_FAMILY, styleFamily());

    xmlWriter.writeStartElement(Elements::STYLE_TABLECOLUMNPROPERTIES);
    xmlWriter.writeAttribute(Attributes::STYLE_USEOPTIMALCOLUMNWIDTH, _useOptimalColumnWidth ? Values::TRUE : Values::FALSE);
    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
}
