#include <QtOdf/Style/TableStyle.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Style;

TableStyle::TableStyle()
{
}

TableStyle::~TableStyle()
{
}

QString TableStyle::styleFamily() const
{
    return Values::TABLE;
}

bool TableStyle::deserializeFrom(QXmlStreamReader &xmlReader)
{
    Q_UNUSED(xmlReader)

    return false;
}

void TableStyle::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    xmlWriter.writeStartElement(Elements::STYLE_STYLE);

    xmlWriter.writeAttribute(Attributes::STYLE_NAME, styleName());
    xmlWriter.writeAttribute(Attributes::STYLE_FAMILY, styleFamily());

    xmlWriter.writeEndElement();
}
