#include <QtOdf/Style/TableCellStyle.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Style;

TableCellStyle::TableCellStyle(QString styleName)
    : AbstractStyle(std::move(styleName))
{
}

TableCellStyle::~TableCellStyle()
{
}

QString TableCellStyle::styleFamily() const
{
    return Values::TABLE_CELL;
}

const QString &TableCellStyle::dataStyleName() const
{
    return _dataStyleName;
}

void TableCellStyle::setDataStyleName(QString dataStyleName)
{
    _dataStyleName = std::move(dataStyleName);
}

bool TableCellStyle::deserializeFrom(QXmlStreamReader &xmlReader)
{
    Q_UNUSED(xmlReader)

    return false;
}

void TableCellStyle::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    xmlWriter.writeStartElement(Elements::STYLE_STYLE);

    xmlWriter.writeAttribute(Attributes::STYLE_NAME, styleName());
    xmlWriter.writeAttribute(Attributes::STYLE_FAMILY, styleFamily());
    if(!_dataStyleName.isEmpty()) {
        xmlWriter.writeAttribute(Attributes::STYLE_DATASTYLENAME, _dataStyleName);
    }

    xmlWriter.writeEndElement();
}
