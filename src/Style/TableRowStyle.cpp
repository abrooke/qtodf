#include <QtOdf/Style/TableRowStyle.h>
#include <QtOdf/Constants.h>

using namespace QtOdf::Style;

TableRowStyle::TableRowStyle()
{
}

TableRowStyle::~TableRowStyle()
{
}

QString TableRowStyle::styleFamily() const
{
    return Values::TABLE_ROW;
}

bool TableRowStyle::deserializeFrom(QXmlStreamReader &xmlReader)
{
    Q_UNUSED(xmlReader)

    return false;
}

void TableRowStyle::serializeTo(QXmlStreamWriter &xmlWriter) const
{
    xmlWriter.writeStartElement(Elements::STYLE_STYLE);

    xmlWriter.writeAttribute(Attributes::STYLE_NAME, styleName());
    xmlWriter.writeAttribute(Attributes::STYLE_FAMILY, styleFamily());

    xmlWriter.writeEndElement();
}
