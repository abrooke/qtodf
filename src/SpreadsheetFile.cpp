#include <QtOdf/Constants.h>
#include <QtOdf/SpreadsheetFile.h>

using namespace QtOdf;

SpreadsheetFile::SpreadsheetFile()
{

}

SpreadsheetFile::~SpreadsheetFile()
{

}

QString SpreadsheetFile::mimeType() const
{
    return MimeTypes::OD_SPREADSHEET;
}

Office::Spreadsheet &SpreadsheetFile::spreadsheet()
{
    return _spreadsheet;
}

const Office::Spreadsheet &SpreadsheetFile::spreadsheet() const
{
    return _spreadsheet;
}

bool SpreadsheetFile::deserializeOfficeBodyContentsFrom(QXmlStreamReader &xmlStreamReader)
{
    while(xmlStreamReader.readNextStartElement()) {
        if(_spreadsheet.deserializeFrom(xmlStreamReader)) {
            return true;
        }
    }

    return false;
}

void SpreadsheetFile::serializeOfficeBodyContentsTo(QXmlStreamWriter &xmlStreamWriter)
{
    _spreadsheet.serializeTo(xmlStreamWriter);
}
