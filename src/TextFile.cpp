#include <QtOdf/Constants.h>
#include <QtOdf/TextFile.h>

using namespace QtOdf;

TextFile::TextFile()
{

}

TextFile::~TextFile()
{

}

QString TextFile::mimeType() const
{
    return MimeTypes::OD_TEXT;
}

Office::Text &TextFile::text()
{
    return _text;
}

const Office::Text &TextFile::text() const
{
    return _text;
}

bool TextFile::deserializeOfficeBodyContentsFrom(QXmlStreamReader &xmlStreamReader)
{
    while(xmlStreamReader.readNextStartElement()) {
        if(_text.deserializeFrom(xmlStreamReader)) {
            return true;
        }
    }

    return false;
}

void TextFile::serializeOfficeBodyContentsTo(QXmlStreamWriter &xmlStreamWriter)
{
    _text.serializeTo(xmlStreamWriter);
}
