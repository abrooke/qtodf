#ifndef QTODF_TEXT_P_H
#define QTODF_TEXT_P_H

#include <QtCore/QXmlStreamWriter>
#include <QtOdf/export.h>

namespace QtOdf::Text {

// http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#element-text_p
class QTODF_EXPORT P
{
public:
    P() = default;

    [[nodiscard]] bool isEmpty() const;
    [[nodiscard]] const QString &text() const;
    void setText(QString text);
    void clearText();

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;

private:
    QString _text;
};

} // namespace QtOdf::Text

#endif // QTODF_TEXT_P_H
