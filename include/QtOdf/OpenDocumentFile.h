#ifndef QTODF_OPEN_DOCUMENT_FILE_H
#define QTODF_OPEN_DOCUMENT_FILE_H

#include <QtCore/QFile>
#include <QtOdf/Office/AutomaticStyles.h>

namespace QtOdf {

class QTODF_EXPORT OpenDocumentFile
{
public:
    explicit OpenDocumentFile();
    virtual ~OpenDocumentFile();

    [[nodiscard]] virtual QString mimeType() const = 0;

    enum class Error {
        NoError,
        FileReadError,
        FileWriteError,
        FileOpenError,
        FilePermissionsError,
        FileUnkownError,
        DeviceNotReadable,
        DeviceNotWritable,
        BadMimetype,
        DeserializationError,
        SerializationError,
    };

    Error load(const QString &fileName, QIODevice::OpenMode mode = QIODevice::ReadOnly);
    Error load(QIODevice &device);

    Error save(const QString &fileName, QIODevice::OpenMode mode = (QIODevice::WriteOnly | QIODevice::Truncate));
    Error save(QIODevice &device);

    [[nodiscard]] Office::AutomaticStyles &automaticStyles();

private:
    struct FileNames {
        static const QString MIMETYPE;
        static const QString MANIFEST_RDF;
        static const QString META_XML;
        static const QString STYLES_XML;
        static const QString CONTENT_XML;
        static const QString SETTINGS_XML;
        static const QString META_INF_MANIFEST_XML;
    };

    virtual bool deserializeOfficeBodyContentsFrom(QXmlStreamReader &xmlStreamReader) = 0;
    virtual void serializeOfficeBodyContentsTo(QXmlStreamWriter &xmlStreamWriter) = 0;

    static void generateMetaInfManifestEntry(QXmlStreamWriter &xmlStreamWriter, const QString &fullPath, const QString &mediaType);

    Office::AutomaticStyles _automaticStyles;
};

} // namespace QtOdf

#endif // QTODF_OPEN_DOCUMENT_FILE_H
