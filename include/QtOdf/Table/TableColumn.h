#ifndef QTODF_TABLE_TABLE_COLUMN_H
#define QTODF_TABLE_TABLE_COLUMN_H

#include <QtCore/QXmlStreamWriter>
#include <QtOdf/export.h>

namespace QtOdf::Table {

// http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#element-table_table-column
class QTODF_EXPORT TableColumn
{
public:
    TableColumn();

    [[nodiscard]] size_t numberColumnsRepeated() const;

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;

private:
    size_t _numberColumnsRepeated;
};

} // namespace QtOdf::Table

#endif // QTODF_TABLE_TABLE_COLUMN_H
