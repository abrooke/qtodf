#ifndef QTODF_TABLE_TABLE_H
#define QTODF_TABLE_TABLE_H

#include <QtOdf/Table/TableColumn.h>
#include <QtOdf/Table/TableRow.h>

namespace QtOdf::Table {

// http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#element-table_table
class QTODF_EXPORT Table
{
public:
    explicit Table(QString tableName = {});

    [[nodiscard]] const QString &name() const;

    [[nodiscard]] size_t rowCount() const;
    [[nodiscard]] size_t columnCount() const;
    /** Get a cell. */
    [[nodiscard]] TableCell *cellAt(size_t row, size_t column);
    /** Get a cell. */
    [[nodiscard]] const TableCell *cellAt(size_t row, size_t column) const;

    /** Get or create a new cell. */
    TableCell &cell(size_t row, size_t column);

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;

private:
    QString _tableName;
    std::vector<TableColumn> _tableColumns;
    std::vector<TableRow> _tableRows;
};

} // namespace QtOdf::Table

#endif // QTODF_TABLE_TABLE_H
