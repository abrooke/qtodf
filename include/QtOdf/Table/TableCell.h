#ifndef QTODF_TABLE_TABLE_CELL_H
#define QTODF_TABLE_TABLE_CELL_H

#include <QtOdf/Text/P.h>

namespace QtOdf::Table {

// http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#element-table_table-cell
class QTODF_EXPORT TableCell
{
public:
    TableCell();

    [[nodiscard]] size_t numberColumnsSpanned() const;
    [[nodiscard]] size_t numberRowsSpanned() const;

    enum ValueType {
        BOOLEAN,
        CURRENCY,
        DATE,
        FLOAT,
        PERCENTAGE,
        STRING,
        TIME,
        VOID,
    };
    [[nodiscard]] ValueType valueType() const;
    [[nodiscard]] QString valueTypeString() const;

    bool booleanValue(bool *ok = nullptr) const;
    double currencyValue(bool *ok = nullptr) const;
    QString currencySymbol(bool *ok = nullptr) const;
    [[nodiscard]] QDate dateValue() const;
    double floatValue(bool *ok = nullptr) const;
    double percentageValue(bool *ok = nullptr) const;
    QString stringValue(bool *ok = nullptr) const;

    void setBooleanValue(bool booleanValue);
    void setCurrencyValue(double numericValue);
    void setDateValue(QDate dateValue);
    void setFloatValue(double floatValue);
    void setPercentageValue(double percentageValue);
    void setStringValue(QString stringValue);
    void setVoidValue();

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;

private:
    static QString valueTypeToString(ValueType valueType);
    static ValueType stringToValueType(QStringView valueTypeAsString);

    QXmlStreamAttributes _attributes;
    size_t _numberColumnsSpanned;
    size_t _numberRowsSpanned;
    Text::P _content;
    ValueType _valueType;
};

} // namespace QtOdf::Table

#endif // QTODF_TABLE_TABLE_CELL_H
