#ifndef QTODF_TABLE_TABLE_ROW_H
#define QTODF_TABLE_TABLE_ROW_H

#include <QtOdf/Table/TableCell.h>

namespace QtOdf::Table {

// http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#element-table_table-row
class QTODF_EXPORT TableRow
{
public:
    TableRow() = default;

    [[nodiscard]] size_t columnCount() const;
    /** Get a cell by column. */
    [[nodiscard]] TableCell *cellAt(size_t column);
    /** Get a cell by column. */
    [[nodiscard]] const TableCell *cellAt(size_t column) const;

    /** Get or create a new cell. */
    TableCell &cell(size_t column);

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;

private:
    std::vector<TableCell> _tableCells;
};

} // namespace QtOdf::Table

#endif // QTODF_TABLE_TABLE_ROW_H
