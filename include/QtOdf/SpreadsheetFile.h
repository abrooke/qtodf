#ifndef QTODF_SPREADSHEET_FILE_H
#define QTODF_SPREADSHEET_FILE_H

#include <QtOdf/OpenDocumentFile.h>
#include <QtOdf/Office/Spreadsheet.h>

namespace QtOdf {

class QTODF_EXPORT SpreadsheetFile : public OpenDocumentFile
{
public:
    SpreadsheetFile();
    ~SpreadsheetFile() override;

    [[nodiscard]] QString mimeType() const final;

    [[nodiscard]] Office::Spreadsheet &spreadsheet();
    [[nodiscard]] const Office::Spreadsheet &spreadsheet() const;

private:
    bool deserializeOfficeBodyContentsFrom(QXmlStreamReader &xmlStreamReader) final;
    void serializeOfficeBodyContentsTo(QXmlStreamWriter &xmlStreamWriter) final;

    Office::Spreadsheet _spreadsheet;
};

} // namespace QtOdf

#endif // QTODF_SPREADSHEET_FILE_H
