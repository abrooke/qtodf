#ifndef QTODF_TEXT_FILE_H
#define QTODF_TEXT_FILE_H

#include <QtOdf/OpenDocumentFile.h>
#include <QtOdf/Office/Text.h>

namespace QtOdf {

class QTODF_EXPORT TextFile : public OpenDocumentFile
{
public:
    TextFile();
    ~TextFile() override;

    [[nodiscard]] QString mimeType() const final;

    [[nodiscard]] Office::Text &text();
    [[nodiscard]] const Office::Text &text() const;

private:
    bool deserializeOfficeBodyContentsFrom(QXmlStreamReader &xmlStreamReader) final;
    void serializeOfficeBodyContentsTo(QXmlStreamWriter &xmlStreamWriter) final;

    Office::Text _text;
};

} // namespace QtOdf

#endif // QTODF_TEXT_FILE_H
