#ifndef QTODF_CONSTANTS_H
#define QTODF_CONSTANTS_H

#include <QtCore/QString>

namespace QtOdf {

struct Attributes
{
    static const QString MANIFEST_FULLPATH;
    static const QString MANIFEST_MEDIATYPE;
    static const QString MANIFEST_VERSION;

    static const QString NUMBER_AUTOMATICORDER;
    static const QString NUMBER_COUNTRY;
    static const QString NUMBER_DECIMALPLACES;
    static const QString NUMBER_GROUPING;
    static const QString NUMBER_LANGUAGE;
    static const QString NUMBER_MINDECIMALPLACES;
    static const QString NUMBER_MININTEGERDIGITS;
    static const QString NUMBER_STYLE;

    static const QString OFFICE_BOOLEANVALUE;
    static const QString OFFICE_CURRENCY;
    static const QString OFFICE_DATEVALUE;
    static const QString OFFICE_VALUE;
    static const QString OFFICE_VALUETYPE;
    static const QString OFFICE_VERSION;

    static const QString STYLE_DATASTYLENAME;
    static const QString STYLE_FAMILY;
    static const QString STYLE_NAME;
    static const QString STYLE_USEOPTIMALCOLUMNWIDTH;

    static const QString TABLE_NAME;
    static const QString TABLE_NUMBERCOLUMNSREPEATED;
    static const QString TABLE_NUMBERCOLUMNSSPANNED;
    static const QString TABLE_NUMBERROWSSPANNED;
    static const QString TABLE_STYLENAME;
};

struct DefaultStyleNames
{
    static const QString TABLE;
    static const QString TABLE_CELL;
    static const QString TABLE_COLUMN;
    static const QString TABLE_ROW;

    static const QString BOOLEAN;
    static const QString CURRENCY;
    static const QString DATE;
    static const QString PERCENTAGE;
    static const QString TIME;
};

struct Elements
{
    static const QString MANIFEST_MANIFEST;
    static const QString MANIFEST_FILEENTRY;

    static const QString NUMBER_BOOLEAN;
    static const QString NUMBER_BOOLEANSTYLE;
    static const QString NUMBER_CURRENCYSTYLE;
    static const QString NUMBER_CURRENCYSYMBOL;
    static const QString NUMBER_DATESTYLE;
    static const QString NUMBER_DAY;
    static const QString NUMBER_HOURS;
    static const QString NUMBER_MINUTES;
    static const QString NUMBER_MONTH;
    static const QString NUMBER_NUMBER;
    static const QString NUMBER_NUMBERSTYLE;
    static const QString NUMBER_PERCENTAGESTYLE;
    static const QString NUMBER_SECONDS;
    static const QString NUMBER_TEXT;
    static const QString NUMBER_TEXTSTYLE;
    static const QString NUMBER_TIMESTYLE;
    static const QString NUMBER_YEAR;

    static const QString OFFICE_AUTOMATICSTYLES;
    static const QString OFFICE_BODY;
    static const QString OFFICE_DOCUMENTCONTENT;
    static const QString OFFICE_FONTFACEDECLS;
    static const QString OFFICE_SCRIPTS;
    static const QString OFFICE_SPREADSHEET;
    static const QString OFFICE_STYLES;
    static const QString OFFICE_TEXT;

    static const QString STYLE_STYLE;
    static const QString STYLE_TABLEPROPERTIES;
    static const QString STYLE_TABLECELLPROPERTIES;
    static const QString STYLE_TABLECOLUMNPROPERTIES;
    static const QString STYLE_TABLEROWPROPERTIES;
    static const QString STYLE_TEXTPROPERTIES;

    static const QString TABLE_TABLE;
    static const QString TABLE_TABLECOLUMN;
    static const QString TABLE_TABLEROW;
    static const QString TABLE_TABLECELL;

    static const QString TEXT_P;
};

struct MimeTypes
{
    static const QString OD_SPREADSHEET;
    static const QString OD_TEXT;
    static const QString RDFXML;
    static const QString TEXT_XML;
};

struct Values
{
public:
    // http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#attribute-office_value-type
    static const QString BOOLEAN;
    static const QString CURRENCY;
    static const QString DATE;
    static const QString FLOAT;
    static const QString PERCENTAGE;
    static const QString STRING;
    static const QString TIME;
    static const QString VOID;

    static const QString TRUE;
    static const QString FALSE;

    // http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#attribute-style_family
    static const QString CHART;
    static const QString DRAWING_PAGE;
    static const QString GRAPHIC;
    static const QString PARAGRAPH;
    static const QString PRESENTATION;
    static const QString RUBY;
    static const QString TABLE;
    static const QString TABLE_CELL;
    static const QString TABLE_COLUMN;
    static const QString TABLE_ROW;
    static const QString TEXT;

    // http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#a19_358number_style
    static const QString LONG;
    static const QString SHORT;
};

} // namespace QtOdf

#endif // QTODF_CONSTANTS_H
