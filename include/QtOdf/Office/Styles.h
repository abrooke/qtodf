#ifndef QTODF_OFFICE_STYLES_H
#define QTODF_OFFICE_STYLES_H

#include <QtOdf/Office/AbstractStyles.h>

namespace QtOdf::Office {

class Styles : public AbstractStyles
{
public:
    [[nodiscard]] QString elementName() const final;

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;
};

} // namespace QtOdf::Office

#endif // QTODF_OFFICE_STYLES_H
