#ifndef QTODF_OFFICE_SPREADSHEET_H
#define QTODF_OFFICE_SPREADSHEET_H

#include <QtCore/QXmlStreamWriter>
#include <QtOdf/Table/Table.h>

namespace QtOdf::Office {

// http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#element-office_spreadsheet
class QTODF_EXPORT Spreadsheet
{
public:
    Spreadsheet();

    /** Get a table by index. */
    [[nodiscard]] Table::Table *tableAt(size_t tableIndex);
    /** Get a table by index. */
    [[nodiscard]] const Table::Table *tableAt(size_t tableIndex) const;
    /** Get a table by name. */
    [[nodiscard]] Table::Table *tableAt(QStringView tableName);
    /** Get a table by name. */
    [[nodiscard]] const Table::Table *tableAt(QStringView tableName) const;

    /** Get or create a new table by name. */
    Table::Table &table(QStringView tableName);
    /** Get or create a new table by name. */
    Table::Table &table(QString tableName);

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;

private:
    std::vector<Table::Table> _tables;
};

} // namespace QtOdf::Office

#endif // QTODF_OFFICE_SPREADSHEET_H
