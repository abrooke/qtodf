#ifndef QTODF_OFFICE_AUTOMATIC_STYLES_H
#define QTODF_OFFICE_AUTOMATIC_STYLES_H

#include <QtOdf/Number/BooleanStyle.h>
#include <QtOdf/Number/CurrencyStyle.h>
#include <QtOdf/Number/DateStyle.h>
#include <QtOdf/Number/PercentageStyle.h>
#include <QtOdf/Number/TimeStyle.h>
#include <QtOdf/Office/AbstractStyles.h>
#include <QtOdf/Style/TableStyle.h>
#include <QtOdf/Style/TableCellStyle.h>
#include <QtOdf/Style/TableColumnStyle.h>
#include <QtOdf/Style/TableRowStyle.h>

namespace QtOdf::Office {

class QTODF_EXPORT AutomaticStyles : public AbstractStyles
{
public:
    AutomaticStyles();
    ~AutomaticStyles() override;

    [[nodiscard]] QString elementName() const final;

    [[nodiscard]] Style::TableStyle &defaultTableStyle();
    [[nodiscard]] Style::TableCellStyle &defaultTableCellStyle();
    [[nodiscard]] Style::TableColumnStyle &defaultTableColumnStyle();
    [[nodiscard]] Style::TableRowStyle &defaultTableRowStyle();

    [[nodiscard]] Number::BooleanStyle &defaultBooleanStyle();
    [[nodiscard]] Number::CurrencyStyle &defaultCurrencyStyle();
    [[nodiscard]] Number::DateStyle &defaultDateStyle();
    [[nodiscard]] Number::PercentageStyle &defaultPercentageStyle();
    [[nodiscard]] Number::TimeStyle &defaultTimeStyle();

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;

private:
    Style::TableStyle _defaultTableStyle;
    Style::TableCellStyle _defaultTableCellStyle;
    Style::TableColumnStyle _defaultTableColumnStyle;
    Style::TableRowStyle _defaultTableRowStyle;
    Number::BooleanStyle _defaultBooleanStyle;
    Number::CurrencyStyle _defaultCurrencyStyle;
    Number::DateStyle _defaultDateStyle;
    Number::PercentageStyle _defaultPercentageStyle;
    Number::TimeStyle _defaultTimeStyle;
};

} // namespace QtOdf::Office

#endif // QTODF_OFFICE_AUTOMATIC_STYLES_H
