#ifndef QTODF_OFFICE_ABSTRACT_STYLES_H
#define QTODF_OFFICE_ABSTRACT_STYLES_H

#include <QtCore/QXmlStreamWriter>
#include <QtOdf/export.h>

namespace QtOdf::Office {

class QTODF_EXPORT AbstractStyles
{
public:
    virtual ~AbstractStyles();
    [[nodiscard]] virtual QString elementName() const = 0;
};

} // namespace QtOdf::Office

#endif // QTODF_OFFICE_ABSTRACT_STYLES_H
