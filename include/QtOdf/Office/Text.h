#ifndef QTODF_OFFICE_TEXT_H
#define QTODF_OFFICE_TEXT_H

#include <QtCore/QXmlStreamWriter>
#include <QtOdf/export.h>

namespace QtOdf::Office {

// http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#element-office_text
class QTODF_EXPORT Text
{
public:
    Text();

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;

private:

};

} // namespace QtOdf::Office

#endif // QTODF_OFFICE_TEXT_H
