#ifndef QTODF_NUMBER_CURRENCY_STYLE_H
#define QTODF_NUMBER_CURRENCY_STYLE_H

#include <QtCore/QLocale>
#include <QtOdf/Number/AbstractNumberStyle.h>

namespace QtOdf::Number {

// http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#element-number_currency-style
class QTODF_EXPORT CurrencyStyle : public AbstractNumberStyle
{
public:
    [[nodiscard]] const QLocale &locale() const;
    void setLocale(QLocale locale);

    [[nodiscard]] const QString &currencySymbol() const;
    void setCurrencySymbol(QString newCurrencySymbol);
    void clearCurrencySymbol();

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;

private:
    QLocale _locale;
    QString _currencySymbol;
};

} // namespace QtOdf::Number

#endif // QTODF_NUMBER_CURRENCY_STYLE_H
