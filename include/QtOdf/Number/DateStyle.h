#ifndef QTODF_NUMBER_DATE_STYLE_H
#define QTODF_NUMBER_DATE_STYLE_H

#include <QtOdf/Number/AbstractNumberStyle.h>

namespace QtOdf::Number {

// http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#element-number_date-style
class QTODF_EXPORT DateStyle : public AbstractNumberStyle
{
public:
    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;
};

} // namespace QtOdf::Number

#endif // QTODF_NUMBER_DATE_STYLE_H
