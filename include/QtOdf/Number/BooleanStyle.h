#ifndef QTODF_NUMBER_BOOLEAN_STYLE_H
#define QTODF_NUMBER_BOOLEAN_STYLE_H

#include <QtOdf/Number/AbstractNumberStyle.h>

namespace QtOdf::Number {

// http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#element-number_boolean-style
class QTODF_EXPORT BooleanStyle : public AbstractNumberStyle
{
public:
    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;
};

} // namespace QtOdf

#endif // QTODF_NUMBER_BOOLEAN_STYLE_H
