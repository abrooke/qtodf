#ifndef QTODF_NUMBER_ABSTRACT_STYLE_H
#define QTODF_NUMBER_ABSTRACT_STYLE_H

#include <QtCore/QXmlStreamWriter>
#include <QtOdf/export.h>

namespace QtOdf::Number {

class QTODF_EXPORT AbstractNumberStyle
{
public:    
    [[nodiscard]] const QString &styleName() const;
    void setStyleName(QString styleName);

private:
    QString _styleName;
};

} // namespace QtOdf::Number

#endif // QTODF_NUMBER_ABSTRACT_STYLE_H
