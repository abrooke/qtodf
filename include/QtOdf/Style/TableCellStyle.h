#ifndef QTODF_STYLE_TABLE_CELL_STYLE_H
#define QTODF_STYLE_TABLE_CELL_STYLE_H

#include <QtOdf/Style/AbstractStyle.h>

namespace QtOdf::Style {

class QTODF_EXPORT TableCellStyle : public AbstractStyle
{
public:
    TableCellStyle(QString styleName = {});
    ~TableCellStyle() override;

    [[nodiscard]] QString styleFamily() const override;

    [[nodiscard]] const QString &dataStyleName() const;
    void setDataStyleName(QString dataStyleName);

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;

private:
    QString _dataStyleName;
};

} // namespace QtOdf::Style

#endif // QTODF_STYLE_TABLE_CELL_STYLE_H
