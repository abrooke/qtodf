#ifndef QTODF_STYLE_TABLE_COLUMN_STYLE_H
#define QTODF_STYLE_TABLE_COLUMN_STYLE_H

#include <QtOdf/Style/AbstractStyle.h>

namespace QtOdf::Style {

class QTODF_EXPORT TableColumnStyle : public AbstractStyle
{
public:
    TableColumnStyle();
    ~TableColumnStyle() override;

    [[nodiscard]] QString styleFamily() const override;

    [[nodiscard]] bool useOptimalColumnWidth() const;
    void setUseOptimalColumnWidth(bool useOptimalColumnWidth);

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;

private:
    bool _useOptimalColumnWidth;
};

} // namespace QtOdf::Style

#endif // QTODF_STYLE_TABLE_COLUMN_STYLE_H
