#ifndef QTODF_STYLE_TABLE_ROW_STYLE_H
#define QTODF_STYLE_TABLE_ROW_STYLE_H

#include <QtOdf/Style/AbstractStyle.h>

namespace QtOdf::Style {

class QTODF_EXPORT TableRowStyle : public AbstractStyle
{
public:
    TableRowStyle();
    ~TableRowStyle() override;

    [[nodiscard]] QString styleFamily() const override;

    bool deserializeFrom(QXmlStreamReader &xmlReader);
    void serializeTo(QXmlStreamWriter &xmlWriter) const;
};

} // namespace QtOdf::Style

#endif // QTODF_STYLE_TABLE_ROW_STYLE_H
