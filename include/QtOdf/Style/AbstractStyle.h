#ifndef QTODF_STYLE_ABSTRACT_STYLE_H
#define QTODF_STYLE_ABSTRACT_STYLE_H

#include <QtCore/QXmlStreamWriter>
#include <QtOdf/export.h>

namespace QtOdf::Style {

class QTODF_EXPORT AbstractStyle
{
public:
    explicit AbstractStyle(QString styleName = {});
    virtual ~AbstractStyle();

    [[nodiscard]] const QString &styleName() const;
    void setStyleName(QString styleName);

    [[nodiscard]] virtual QString styleFamily() const = 0;

private:
    QString _styleName;
};

} // namespace QtOdf::Style

#endif // QTODF_STYLE_ABSTRACT_STYLE_H
