QT += core gui-private
CONFIG += c++17

INCLUDEPATH += "$$PWD/include"
INCLUDEPATH += "$$PWD/include/qmake_intree"
SOURCES += $$files("$$PWD/src/*.cpp", true)
